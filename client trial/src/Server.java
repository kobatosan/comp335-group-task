// Websites accessed: 27/03/19
// Code derived from: https://stackoverflow.com/questions/20631666/java-socket-programming-conversation
// https://www.geeksforgeeks.org/socket-programming-in-java/

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    private PrintStream print;

    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception{
        Server server = new Server(); // declaring
        server.start();
    }

    /**
     * @throws Exception
     */
    public void start()
            throws Exception{ // to catch for errors earlier in the program and to be handled by JVM

        InetAddress address = InetAddress.getByName("127.0.0.1");  // establish connection to Client IP; https://docs.oracle.com/javase/8/docs/api/java/net/InetAddress.html#getByName-java.lan.String-
        ServerSocket serverSocket = new ServerSocket(8096, 1, address); // expect connection to one server, hence '1'

        //  ServerSocket socket = new ServerSocket(8096);
        Socket soc = serverSocket.accept();
        InputStreamReader iReader = new InputStreamReader(soc.getInputStream());
        BufferedReader bReader = new BufferedReader(iReader); // puts server message into buffer

        print = new PrintStream(soc.getOutputStream());

        while (true){
            String clientMessage =  bReader.readLine();
            System.out.println(clientMessage);


            if(clientMessage.trim().equalsIgnoreCase("hi")){
                print.println("hi");
                print.println("Waiting for connection");

            }

            if(clientMessage.trim().equalsIgnoreCase("HELO")) {
                print.println("OK");
            }
            if (clientMessage.trim().equalsIgnoreCase("bye")){
                break;
            }
//            else{
//                if(isPrime(Integer.parseInt(receivedMsg))){
//                    print.println("Yes - it is a prime number :)");
//                }else{
//                    print.println("No - it is not a prime number :(");
//                }
//                print.println("enter number to check: ");
//            }
        }
    }

    private boolean isPrime(int n) {
        if (n==1) return false;

        for(int i=2;i<n;i++) {
            if(n%i==0){
                return false;
            }
        }
        return true;
    }

}